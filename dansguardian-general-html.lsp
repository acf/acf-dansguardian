<% local form, viewlibrary, page_info, session = ...
htmlviewfunctions = require("htmlviewfunctions")
%>

<% if viewlibrary and viewlibrary.dispatch_component then
	viewlibrary.dispatch_component("status")
end %>

<% local header_level = htmlviewfunctions.displaysectionstart(cfe({label="Configuration"}), page_info) %>
<%
	htmlviewfunctions.displayformstart(form, page_info)
	for field,val in pairs(form.value) do
		val.name = field
	end
%>

<% local header_level2 = htmlviewfunctions.displaysectionstart(cfe({label="General"}), page_info, htmlviewfunctions.incrementheader(header_level)) %>
<p>
These parameters define the interface and port that Dansguardian uses to accept connections.
</p>

<%
	htmlviewfunctions.displayformitem(form.value.filterip)
	htmlviewfunctions.displayformitem(form.value.filterport)
%>

<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionstart(cfe({label="Proxy service"}), page_info, header_level2) %>
<p>
These parameters define the ip address and port that Dansguardian should forward requests on to.
</p>

<%
	htmlviewfunctions.displayformitem(form.value.proxyip)
	htmlviewfunctions.displayformitem(form.value.proxyport)
%>

<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displaysectionstart(cfe({label="Filter Actions"}), page_info, header_level2) %>
<p>
These parameters define how sensitive the filter is, and where to redirect requests if the content filter
determines that the content is inappropriate. The "naughtynesslimit" is more sensitive the lower it is set.
The author recommends 50 for "young children", 100 for "older children" and 160 for "young adults".
</p>

<%
	htmlviewfunctions.displayformitem(form.value.accessdeniedaddress)
	htmlviewfunctions.displayformitem(form.value.naughtynesslimit)
%>

<% htmlviewfunctions.displaysectionend(header_level2) %>
<% htmlviewfunctions.displayformend(form) %>
<% htmlviewfunctions.displaysectionend(header_level) %>

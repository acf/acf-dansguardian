-- the squid  controller
local mymodule = {}

mymodule.default_action = "status"

mymodule.status = function( self )
	return self.model.get_status()
end

mymodule.startstop = function( self )
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

mymodule.general = function( self )
	return self.handle_form(self, self.model.read_general_config, self.model.update_general_config, self.clientdata, "Save", "Edit General Configuration", "General Configuration Set")
end

mymodule.listfiles = function( self )
	return self.model.list_files()
end

mymodule.listconfigfiles = function( self )
	return self.model.list_config_files()
end

mymodule.edit = function( self )
	return self.handle_form(self, function() return self.model.get_file(self.clientdata.filename) end, self.model.update_file, self.clientdata, "Save", "Edit File", "File Saved")
end

return mymodule

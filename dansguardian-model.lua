-- Copyright(c) 2007 A. Brodmann - Licensed under terms of GPL2
local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
posix = require("posix")
fs = require("acf.fs")
format = require("acf.format")
validator = require("acf.validator")

-- Set variables
dansguardiancfg = "/etc/dansguardian/dansguardian.conf"
dansguardiancfg2 = "/etc/dansguardian/dansguardianf1.conf"
local processname = "dansguardian"
local packagename = "dansguardian"
local baseurl = "/etc/dansguardian"

--[[
local categoryfiles = {
	['weighted'] = tostring(baseurl .. "/weightedphraselist"),
	['banned'] = tostring(baseurl .. "/bannedphraselist"),
	['exception'] = tostring(baseurl .. "/exceptionphraselist"),
	}

-- ################################################################################
-- LOCAL FUNCTIONS

local function get_includes_from_file(file)
	local retval = {}
	for k,v in pairs(fs.read_file_as_array(file) or {}) do
		if (string.match(v, '^%s*.Include')) then
			local val = string.match(v,'^%s*.Include<(.*)>%s*')
			retval[val] = true
		end
	end
	return retval
end

local categoryfilecontent = {
	['weighted'] = get_includes_from_file(categoryfiles['weighted']),
	['banned'] = get_includes_from_file(categoryfiles['banned']),
	['exception'] = get_includes_from_file(categoryfiles['exception']),
	}
--]]

local validate_general_config = function( config )
	local success = true
	if config.value.filterip.value ~= "" and not validator.is_ipv4(config.value.filterip.value) then
		config.value.filterip.errtxt = "Invalid IP address"
		success = false
	end
	if not validator.is_port(config.value.filterport.value) then
		config.value.filterport.errtxt = "Invalid port"
		success = false
	end
	if not validator.is_ipv4(config.value.proxyip.value) then
		config.value.proxyip.errtxt = "Invalid IP address"
		success = false
	end
	if not validator.is_port(config.value.proxyport.value) then
		config.value.proxyport.errtxt = "Invalid port"
		success = false
	end
	-- FIXME don't know how to validate accessdeniedaddress
	if not validator.is_integer(config.value.naughtynesslimit.value) then
		config.value.naughtynesslimit.errtxt = "Invalid number"
		success = false
	end

	return success, config
end

local is_valid_filename = function(filename)
	local dirname = posix.dirname(filename)
	return validator.is_valid_filename(filename) and string.match(dirname, baseurl) and not string.match(dirname, "%.%.")
end

-- ################################################################################
-- PUBLIC FUNCTIONS

mymodule.get_status = function()
	return modelfunctions.getstatus(processname, packagename, "Dans Guardian Status")
end

function mymodule.get_startstop(self, clientdata)
	return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
	return modelfunctions.startstop_service(startstop, action)
end

mymodule.read_general_config = function()
	local retval = { filterip = cfe({ label="Filter IP", descr="Leave blank to listen on all IPs" }),
		filterport = cfe({ label="Filter Port" }),
		proxyip = cfe({ label="Proxy IP" }),
		proxyport = cfe({ label="Proxy Port" }),
		accessdeniedaddress = cfe({ label="AccessDeniedAddress" }),
		naughtynesslimit = cfe({ label="NaughtynessLimit" })
	}

	local config = format.parse_ini_file(fs.read_file(dansguardiancfg) or "", "")
	if config then
		if config.filterip then	retval.filterip.value = config.filterip end
		if config.filterport then retval.filterport.value = config.filterport end
		if config.proxyip then retval.proxyip.value = config.proxyip end
		if config.proxyport then retval.proxyport.value = config.proxyport end
		if config.accessdeniedaddress then retval.accessdeniedaddress.value = string.sub(config.accessdeniedaddress, 2, -2) end
	end

	config = format.parse_ini_file(fs.read_file(dansguardiancfg2) or "", "")
	if config then
		if config.naughtynesslimit then retval.naughtynesslimit.value = config.naughtynesslimit end
	end

	return cfe({ type="group", value=retval, label="Dansguardian General Config" })
end

mymodule.update_general_config = function(self, config)
	local success, config = validate_general_config(config)

	if success then
		local text = fs.read_file(dansguardiancfg) or ""

		text = format.update_ini_file(text, "", "filterip", config.value.filterip.value)
		text = format.update_ini_file(text, "", "filterport", config.value.filterport.value)
		text = format.update_ini_file(text, "", "proxyip", config.value.proxyip.value)
		text = format.update_ini_file(text, "", "proxyport", config.value.proxyport.value)
		text = format.update_ini_file(text, "", "accessdeniedaddress", "'"..config.value.accessdeniedaddress.value.."'")
		fs.write_file(dansguardiancfg, text)

		text = fs.read_file(dansguardiancfg2) or ""
		text = format.update_ini_file(text, "", "naughtynesslimit", config.value.naughtynesslimit.value)
		fs.write_file(dansguardiancfg2, text)
	else
		config.errtxt = "Failed to set config"
	end

	return config
end

mymodule.get_file = function(filename)
	return modelfunctions.getfiledetails(filename, is_valid_filename)
end

mymodule.update_file = function(self, filedetails)
	return modelfunctions.setfiledetails(self, filedetails, is_valid_filename)
end

mymodule.list_files = function()
	local retval = {}
	for file in fs.find(null, baseurl) do
		local details = posix.stat(file)
		if details.type == "regular" and not string.match(file, "logrotation$") and not string.match(file, "%.conf$") and not string.match(file, "%.gif$") then
			details.filename = file
			table.insert(retval, details)
		end
	end
	table.sort(retval, function(a,b) return (a.filename < b.filename) end)
	return cfe({ type="structure", value=retval, label="List of Dansguardian files" })
end

mymodule.list_config_files = function()
	local list = {}
	local details = posix.stat(dansguardiancfg) or {}
	details.filename = dansguardiancfg
	list[1] = details
	details = posix.stat(dansguardiancfg2) or {}
	details.filename = dansguardiancfg2
	list[2] = details
	return cfe({ type="structure", value=list, label="List of Dansguardian config files" })
end

--[[
get_categories = function()

	local retval = {}
	local entries = posix.dir( baseurl .. "/phraselists" )

	for k,v in ipairs( entries ) do
		local attrs = posix.stat( baseurl .. "/phraselists/" .. v )

		if attrs.type == "directory" and v ~= "." and v ~= ".." then
			local entrycontent = {}
			local someactive = false
			for k1,v1 in pairs(posix.dir( baseurl .. "/phraselists/" .. v )) do
				if not string.match(v1, "^%.") then
					local active
					if (categoryfilecontent[string.match(v1,'%w*')][baseurl .. "/phraselists/" .. v .."/" .. v1]) then
						active = true
						someactive = true
					end
					table.insert(entrycontent, {name=v1, active=active, mhdebug=string.match(v1,'%w*'),mhdebug2=baseurl .. "/phraselists/" .. v .."/" .. v1})
				end
			end
			table.insert( retval, {name=v, option=entrycontent, active=someactive} )
		end
	end

	return retval
end

get_category = function(category, object)
	local filename = baseurl .. "/phraselists/" .. category .. "/" .. object

	if not (categoryfilecontent[string.match(object,'%w*')]) or
	not (fs.is_file(filename)) or
	not (validator.is_valid_filename(filename, baseurl)) then
		return nil, "Something went wrong."
	end

	local retval = {}

	retval.activate = cfe({
		label="Activate this object",
		type="checkbox",
		checked="yes",
		})
	if not (categoryfilecontent[string.match(object,'%w*')][filename]) then
		retval.activestatus = cfe({
			label="Category object is currently",
			errtxt = "Deactivated",
			})
		retval.activate.checked=nil
	end

	retval.configfile = cfe({
		label="Activation is done in",
		value=categoryfiles[string.match(object,'%w*')],
		})

	local filedetails = fs.stat(filename)
	retval.mtime = cfe({
		label="Modify date",
		value=filedetails.mtime,
		})

	retval.category = cfe({
		name="category",
		label="Category name",
		value=category,
		})

	retval.name = cfe({
		name="name",
		label="Object name",
		value=object,
		})

	retval.filename = cfe({
		name="filename",
		label="Filename",
		type="hidden",
		value=filename,
		})
	retval.filecontent = cfe({
		label="Filename",
		value=fs.read_file(filename) or "",
		type="longtext",
		})

	return retval
end
--]]

return mymodule
